export const CART_TASTIC_TEST_IDS = {
    priceWithoutShippingText: 'cart-price-without-shipping-text',
    removeConfirmButton: 'remove-from-cart-confirm-button',
    shippingCostText: 'cart-shipping-cost-text',
    showVoucherFieldCheckbox: 'show-voucher-field-checkbox',
    totalPriceWithShippingText: 'cart-total-price-with-shipping-text',
    voucherErrorAlert: 'voucher-error-alert',
    voucherInputField: 'voucher-input-field',
    voucherRedeemButton: 'voucher-redeem-button',
}
