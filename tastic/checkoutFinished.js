export const CHECKOUT_FINISHED_TASTIC_TEST_IDS = {
    shippingAddress: 'shipping-address-checkout-finished-div',
    billingAddress: 'billing-address-checkout-finished-div',
    nameLineAddress: 'name-line-address-div',
    streetLineAddress: 'street-line-address-div',
    cityLineAddress: 'city-line-address-div',
    extraAddressInfoLine: 'extra-line-address-div',
    extraStreetInfoLine: 'extra-street-line-address-div',
    orderId: 'order-id-checkout-finished-text',
    orderProductLine: 'checkout-finished-product-line-including-name-',
    orderProductName: 'checkout-finished-product-name-text',
}
