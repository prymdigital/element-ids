export const SINGLE_PRODUCT_DETAIL_COMPONENT_TEST_IDS = {
    singleProductAttributeContainer: 'single-product-attribute-container',
    singleProductAttributeLabel: 'single-product-attribute-label',
    singleProductAttributeRowPrefix: 'single-product-attribute-row',
    singleProductAttributeValue: 'single-product-attribute-value',
    singleProductDescriptionContainer: 'single-product-description-container',
    singleProductDescriptionTitle: 'single-product-description-title-text',
    singleProductDescriptionContent: 'single-product-description-content-text',
    singleProductName: 'single-product-name-text',
}
