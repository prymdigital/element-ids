export const USER_COMPONENT_TEST_IDS = {
    forgotPasswordLink: 'forgot-password-link',
    loginButton: 'login-button',
    loginEmailInput: 'login-email-input',
    loginPasswordInput: 'login-password-input',
    registerButton: 'register-button',
}
