export const PRODUCT_AVAILABILITY_SUBSCRIPTION_COMPONENT_TEST_IDS = {
    formCheckbox: 'product-availability-subscription-form-checkbox',
    formSubmitButton: 'product-availability-subscription-form-submit-button',
    formEmailInput: 'product-availability-subscription-form-email-input',
    formMessageContainer: 'product-availability-subscription-form-message-container',
}
